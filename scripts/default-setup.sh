#!/usr/bin/fish

set script_dir (dirname (status --current-filename))
set -x ARCASTRO_LINUX_SETUP_ROOT (dirname "$script_dir")
echo "\$ARCASTRO_LINUX_SETUP_ROOT is $ARCASTRO_LINUX_SETUP_ROOT"

eval "$ARCASTRO_LINUX_SETUP_ROOT/scripts/set-custom-fish-config.sh"
eval "$ARCASTRO_LINUX_SETUP_ROOT/scripts/set-custom-tmux-config.sh"
