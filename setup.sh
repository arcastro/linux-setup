#!/bin/bash

source setup.config

ARCASTRO_LINUX_SETUP_ROOT=$(pwd)

if [ "$RUN_APT_GET_UPGRADE" = 1 ]; then
    apt-get update && apt-get upgrade
fi


if [ "$INSTALL_TMUX" = 1 ]; then
    ! ( type tmux &> /dev/null ) && apt-get install tmux
fi


if [ "$INSTALL_FISH_SHELL" = 1 ]; then

    ! ( type fish &> /dev/null ) && apt-get install fish

    if [ "$RUN_FISH_AT_LOGIN" = 1 ]; then
        CONTENTS="\n"
        CONTENTS="$CONTENTS""# Run Fish Shell with every new session\n"
        CONTENTS="$CONTENTS""fish"
        CONTENTS="$(echo -e $CONTENTS)"
        echo "$CONTENTS" >> ~/.bashrc
    fi

    if [ "$SET_CUSTOM_FISH_CONFIG" = 1 ]; then
	source scripts/set-custom-fish-config.sh
    fi
fi

if [ "$INSTALL_GIT" = 1 ]; then
    ! ( type git &> /dev/null ) && apt-get install git
fi

