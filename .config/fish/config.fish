########################
# Fish-Related Aliases #
########################

alias refresh-aliases ". ~/.config/fish/config.fish"
alias aliases "vi ~/.config/fish/config.fish; and refresh-aliases"

#######
# GIT #
#######

alias gba "git branch -avv"

########
# TMUX #
########

function tmux-prefix
    tmux unbind C-b
    tmux set -g prefix C-$argv[1]
    tmux bind C-$argv[1] send-prefix
end

alias linux-setup "tmux attach -t linux-setup"

##########
# DOCKER #
##########

function remove-dangling-volumes

    # I haven't verified the safety of this image.
    # It could be malicious.

    docker run \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v /var/lib/docker:/var/lib/docker \
        --rm martin/docker-cleanup-volumes
end

function psql-sandbox
    echo "Starting instance of PSQL."
    docker run --name psql-sandbox -d -e POSTGRES_PASSWORD="psql-sandbox" -e POSTGRES_USER="psql-sandbox" postgres

    while not docker exec -it psql-sandbox psql -U psql-sandbox
      echo "Trying again in 2 seconds."
      sleep 2;
    end

    echo "Removing instance of PSQL."
    docker stop psql-sandbox
    docker rm psql-sandbox
end

###########
# SERVERS #
###########

alias stack "tmux attach -t stack"
alias s1.digitalocean.servers.alanrcastro.com "ssh [INSERT IP HERE]"
alias s1.vpscheap.servers.alanrcastro.com "ssh 199.175.49.146"
alias s1.quadhost.servers.alanrcastro.com-S "ssh 104.171.126.3"
alias s2.quadhost.servers.alanrcastro.com-M "ssh 104.171.126.4"
alias s3.quadhost.servers.alanrcastro.com-L "ssh 104.171.123.122"

#########
# OTHER #
#########

alias ip-address "curl http://ipinfo.io/ip"

function ignore-stderr
    eval $argv 2> /dev/null
end

function view-open-ports
    if [ (count $argv) = 1 ]; and [ $argv[1] = "-q" ]
        view-open-ports | grep -o -P '(?<=:)\d+' | cat | sort -g | uniq
    else
        ignore-stderr netstat -ntlp | grep LISTEN | cat
    end
end

##########
# AMAZON #
##########

alias bbc "brazil-build clean"
alias bb "bbc; and brazil-build"
alias bba "bb; and brazil-build apollo-pkg"
alias ubuntu "ssh uf8bc12710091560c6707.ant.amazon.com -t 'fish'"

function datanet-backfill-weekly
    if [ (count $argv) = 5 ]
        /apollo/env/DatanetBackfill/bin/backfiller \
            --backfill-name $argv[1] \
            --extract-job-id $argv[2]\
            --load-job-id $argv[3] \
            --from-date $argv[4] \
            --through-date $argv[5] \
            --max-extract-errors 15 \
            --max-load-errors 100 \
            --interval weekly \
            --weekly-day Saturday
    else
        /apollo/env/DatanetBackfill/bin/backfiller \
            --backfill-name $argv[1] \
            --extract-job-id $argv[2]\
            --no-load \
            --from-date $argv[3] \
            --through-date $argv[4] \
            --max-extract-errors 15 \
            --max-load-errors 100 \
            --interval weekly \
            --weekly-day Saturday
    end
end

function datanet-backfill-monthly
    /apollo/env/DatanetBackfill/bin/backfiller \
        --backfill-name $argv[1] \
        --extract-job-id $argv[2]\
        --load-job-id $argv[3] \
        --from-date $argv[4] \
        --through-date $argv[5] \
        --max-extract-errors 15 \
        --max-load-errors 100 \
        --interval monthly
end

